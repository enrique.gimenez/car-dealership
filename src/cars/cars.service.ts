import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

import { Car } from './cars.interface';
import { CreateCarDto, UpdateCarDto } from './dto';

@Injectable()
export class CarsService {
  private cars: Car[] = [];

  findAll() {
    return this.cars;
  }

  findOneById(id: string) {
    const car = this.cars.find((car) => car.id === id);
    if (!car) throw new NotFoundException(`Car with id ${id} not found.`);
    return car;
  }

  findByBrand(brand: string) {
    return this.cars.filter((car) => car.brand === brand);
  }

  findByModel(model: string) {
    return this.cars.filter((car) => car.model === model);
  }

  create(car: CreateCarDto) {
    const newCar = {
      id: uuid(),
      ...car,
    };
    this.cars.push(newCar);
    return newCar;
  }

  update(updateCarDto: UpdateCarDto, id: string) {
    let toUpdate = this.findOneById(id);
    this.cars = this.cars.map((car) => {
      if (car.id === id) {
        toUpdate = {
          ...toUpdate,
          ...updateCarDto,
          id,
        };
        return toUpdate;
      }
      return car;
    });
    return toUpdate;
  }

  delete(id: string) {
    this.findOneById(id);
    this.cars = this.cars.filter((car) => car.id !== id);
    return this.cars;
  }

  fillCarsWithSeed(cars: Car[]) {
    this.cars = cars;
  }
}
