import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
} from '@nestjs/common';

import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}
  @Get()
  getAllCars() {
    return this.carsService.findAll();
  }

  @Get(':id')
  getCarById(@Param('id', ParseUUIDPipe) id: string) {
    return this.carsService.findOneById(id);
  }

  @Post()
  create(@Body() newCar: CreateCarDto) {
    return this.carsService.create(newCar);
  }

  @Patch(':id')
  update(@Body() updateCar: UpdateCarDto, @Param('id') id: string) {
    return this.carsService.update(updateCar, id);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.carsService.delete(id);
  }
}
