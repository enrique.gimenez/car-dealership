import { v4 as uuid } from 'uuid';

import { Car } from 'src/cars/cars.interface';

export const CARS_SEED: Car[] = [
  {
    id: uuid(),
    brand: 'Starlet',
    model: 'toyota',
  },
  {
    id: uuid(),
    brand: 'Fun Cargo',
    model: 'toyota',
  },
  {
    id: uuid(),
    brand: 'Ecosport',
    model: 'Ford',
  },
];
